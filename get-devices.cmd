@echo off
rem setlocal enableextensions
rem chcp 65001

set device=0
set outp=
set outptmp=

:loop
for /f "tokens=2*" %%a in ('reg.exe query "HKLM\SOFTWARE\WOW6432Node\Audible\SWGIDMAP" /v "%device%"') do ( set bytes=%%b )
set activation_bytes=%bytes:~6,2%%bytes:~4,2%%bytes:~2,2%%bytes:~0,2%
rem if /i "%bytes%" EQU "" echo Device %device% does not exist! & goto nxt
rem if /i "%activation_bytes%" EQU "FFFFFFFF" echo Device %device% is not activated! & goto nxt
rem echo Using Device %device% (Activation Bytes: %activation_bytes%)

if /i "%bytes%" EQU "" goto nxt
if /i "%activation_bytes%" EQU "FFFFFFFF" goto nxt


set outptmp=%outp%
set outp=%outptmp%,%activation_bytes%
:nxt
set /a device=device+1
if %device%==8 goto eof
goto loop

:eof
echo %outp:~1%