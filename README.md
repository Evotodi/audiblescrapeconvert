**Audible Scraper Converter (ACS)**
==============================

ACS is a python script I put together using tidbits found around the web for converting aax files to m4a, logging into amazon, and scraping web pages. I wrote this smashup to facilitate backing up my rather large library of audio books so please keep in mind your mileage may vary.

## Requirements ##
* Windows OS
* Audible Download Manager
* Python >= 3.3

#### Python Modules ####
* beautifulsoup4==4.6.0
* pyparsing==2.2.0
* pypiwin32==220
* python-dateutil==2.6.0
* python-registry==1.0.4
* pywinauto==0.6.2
* requests==2.13.0
* selenium==3.4.1
* shellescape==3.4.1
* six==1.10.0
* termcolor==1.1.0

## How to use ##

**Before running ACS run audibledownloadmanager.exe and select the folder where you want adm to download to!**

ACS runs in different modes, and you can only specify one mode at a time.
````python
usage: scraper.py [-h] [-f] [-s] [-u] [-c] [-k] [-l] [-d]

Select only one of the modes below.

optional arguments:
  -h, --help         show this help message and exit

Modes:
  -f, --full         Do the full process
  -s, --scrape       Only scrape Audible and update booksOnline.json
  -u, --update       Update the booksDownloaded.json and booksConverted.json
                     files. Will update online if booksOnline.json is older
                     that 24 hrs.
  -c, --convert      Only do conversions
  -k, --list_only    Get list of books needing downloaded
  -l, --list_update  Update then get list of books needing downloaded
  -d, --download     Download books missing from converted list

````

When you run scraper.py for the first time it will create a settings.json and exit. Open the settings.json file and fill in the missing values.

In the setting file is MaxConversionsPerRun and MaxDownloadsPerRun setting them to a number greater than 0 will limit the conversions and downloads per execution of scraper.py

## Notes ##
I have found downloading a large (200+) books at a time can cause the process which waits for the downloads to finish error out. This is not a problem just re-run with -c after adm has finished all of its downloads.


## Thanks ##
Like a said above this script is a mashup of tidbits around the web, and I unfortunately did not keep track of the authors of those tidbit, for this I apologize to them. I you find some code that looks like something you wrote, then thank you!



If you find some better ways of accomplishing the tasks please let me know.