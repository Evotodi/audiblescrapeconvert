from __future__ import print_function
import time
from selenium import webdriver
from urllib.parse import urlencode
import os, sys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException
from bs4 import BeautifulSoup
import json
import subprocess
import pyexifinfo
import re
import tempfile
from random import randint
from dateutil.parser import *
import shutil
from termcolor import colored
import argparse
from pywinauto.application import Application

class AudibleConverter():
    audibleAaxPath = None
    audibleM4aPath = None
    audibleDownloadHelperPath = None
    tempdir = os.path.join(tempfile.gettempdir(), 'audible_convert')
    username = None
    password = None
    booksOnline = {}
    booksDownloaded = {}
    booksConverted = {}
    activationCodes = []
    maxConversionsPerRun = 0
    maxDownloadsPerRun = 0
    booksDownloadedJson = 'booksDownloaded.json'
    booksOnlineJson = 'booksOnline.json'
    booksConvertedJson = 'booksConverted.json'
    booksToDownloadJson = 'booksToDownload.json'

    def __init__(self):
        self.setup()

        if os.path.isfile(self.booksOnlineJson):
            self.loadBooksOnlineDict()

        if os.path.isfile(self.booksDownloadedJson):
            self.loadBooksDownloadDict()

        if os.path.isfile(self.booksConvertedJson):
            self.loadBooksConvertDict()

        if not os.path.isdir(self.tempdir):
            os.makedirs(self.tempdir)

        if not os.path.isfile(self.audibleDownloadHelperPath):
            raise Exception("Could not find AudibleDownloadHelper.exe")
    #
    # Downloaded
    #
    def getDownloadedBooks(self, quiet = False):

        if not quiet:
            print("Getting downloaded books")

        books = {}
        addedBooks = {}
        matches = []
        for root, dirnames, filenames in os.walk(self.audibleAaxPath):
            filenames = [f for f in filenames if os.path.splitext(f)[1] in '.aax']
            for filename in filenames:
                matches.append(os.path.join(root, filename))

        cnt = len(matches)
        for file in matches:
            exif = pyexifinfo.get_json(file)
            if not quiet:
                i = "Getting Meta: {}".format(cnt)
                sys.stdout.write("\r" + i)
                sys.stdout.flush()
            try:
                prodid = exif[0]['QuickTime:ProductID']
                book = {prodid:{
                    'title': str(exif[0]['QuickTime:Title']).replace(' (Unabridged)', ''),
                    'author': exif[0]['QuickTime:Artist'],
                    'length': exif[0]['QuickTime:Duration'],
                    'date': exif[0]['QuickTime:ReleaseDate'],
                    'prodid': exif[0]['QuickTime:ProductID'],
                    'fname': os.path.basename(file)
                }}
            except KeyError as k:
                print()
                print(colored("FAILURE = PRODID: {} BOOK {}".format(prodid, book), 'red'))
                print(colored(k, 'red'))
                continue


            if len(self.booksDownloaded) != 0:
                if not prodid in self.booksDownloaded:
                    addedBooks.update(book)
                    books.update(book)
            else:
                books.update(book)
            cnt -= 1
        for nb in books:
            self.booksDownloaded[nb] = books[nb]
        self.saveBooksDownloadDict()
        if not quiet:
            print('')
            print("Downloaded Books Added to json: {}".format(addedBooks))

    def saveBooksDownloadDict(self):
        if len(self.booksDownloaded) == 0:
            return False
        with open(self.booksDownloadedJson, 'w') as fp:
            json.dump(self.booksDownloaded, fp, indent=4)
        return True

    def loadBooksDownloadDict(self):
        with open(self.booksDownloadedJson, 'r') as fp:
            data = json.load(fp)
        self.booksDownloaded = data
        return data

    def setBooksDownloaded(self, books):
        self.booksDownloaded = books

    def getBooksDownloaded(self):
        return self.booksDownloaded
    #
    # Converted
    #
    def getConvertedBooks(self):
        print("Getting converted books")
        books = {}
        addedBooks = {}
        matches = []
        for root, dirnames, filenames in os.walk(self.audibleM4aPath):
            filenames = [f for f in filenames if os.path.splitext(f)[1] in '.m4a']
            for filename in filenames:
                matches.append(os.path.join(root, filename))

        cnt = len(matches)
        for file in matches:
            exif = pyexifinfo.get_json(file)
            i = "Getting Meta: {}".format(cnt)
            sys.stdout.write("\r" + i)
            sys.stdout.flush()
            prodid = str(exif[0]['XMP:ProductID']).replace('"', '')
            book = {prodid: {
                'title': str(exif[0]['QuickTime:Title']).replace(' (Unabridged)', ''),
                'author': exif[0]['QuickTime:Artist'],
                'length': exif[0]['QuickTime:Duration'],
                'date': exif[0]['XMP:ReleaseDate'],
                'prodid': str(exif[0]['XMP:ProductID']).replace('"', ''),
                'fname': os.path.basename(file)}}

            if len(self.booksConverted) != 0:
                if not prodid in self.booksConverted:
                    addedBooks.update(book)
                    books.update(book)
            else:
                books.update(book)
            cnt-=1
        for nb in books:
            self.booksConverted[nb] = books[nb]
        self.saveBooksConvertDict()
        print('')
        print("Converted Books Added to json: {}".format(addedBooks))
        # print("Converted Books: {}".format(self.booksConverted))

    def saveBooksConvertDict(self):
        if len(self.booksDownloaded) == 0:
            return False
        with open(self.booksConvertedJson, 'w') as fp:
            json.dump(self.booksConverted, fp, indent=4)
        return True

    def loadBooksConvertDict(self):
        with open(self.booksConvertedJson, 'r') as fp:
            data = json.load(fp)
        self.booksConverted = data
        return data

    def setBooksConverted(self, books):
        self.booksConverted = books

    def getBooksConverted(self):
        return self.booksConverted
    #
    #Online
    #
    def getOnlineLibrary(self, force = False):
        print("Getting online library books")
        if not force:
            if os.path.isfile(self.booksOnlineJson):
                print("Current Time (secs):             {}".format(int(time.time())))
                print("{} modified time (secs): {}".format(self.booksOnlineJson.title(), int(os.path.getmtime(self.booksOnlineJson))))
                tdiff = int(time.time()) - int(os.path.getmtime(self.booksOnlineJson))
                print("{} time diff (secs):     {}".format(self.booksOnlineJson.title(), tdiff))

                if tdiff <= int(8640000):
                    print("Using local library")
                    return self.loadBooksOnlineDict()

        if not self.username or not self.password:
            raise Exception("Username or Password not set")

        base_url = 'https://www.audible.com/'
        chromedriver_path = "chromedriver.exe"
        debug = True

        durl = None #'http://127.0.0.1:63425'
        sess = None #'2bad4542a498834dc4b75f1a53d8a4fa'

        if sess is None:
            opts = webdriver.ChromeOptions()
            opts.add_argument(
                "user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0")

            if '@' in self.username:  # Amazon login using email address
                login_url = "https://www.amazon.com/ap/signin?"
            else:  # Audible member login using username (untested!)
                login_url = "https://www.audible.com/sign-in/ref=ap_to_private?forcePrivateSignIn=true&rdPath=https%3A%2F%2Fwww.audible.com%2F%3F"

            # player_id = base64.encodestring(hashlib.sha1("".encode('utf8')).digest()).rstrip()  # keep this same to avoid hogging activation slots
            player_id = b'2jmj7l5rSw0yVb/vlWAYkK/YBwk='
            print("[*] Player ID is %s" % player_id)  # 2jmj7l5rSw0yVb/vlWAYkK/YBwk=

            payload = {
                'openid.ns': 'http://specs.openid.net/auth/2.0',
                'openid.identity': 'http://specs.openid.net/auth/2.0/identifier_select',
                'openid.claimed_id': 'http://specs.openid.net/auth/2.0/identifier_select',
                'openid.mode': 'logout',
                'openid.assoc_handle': 'amzn_audible_us',
                'openid.return_to': base_url + 'player-auth-token?playerType=software&playerId=%s=&bp_ua=y&playerModel=Desktop&playerManufacturer=Audible' % (
                player_id)
                # 'openid.return_to': base_url + 'player-auth-token?playerType=software&playerId=%s=&bp_ua=y&playerModel=Desktop&playerManufacturer=Audible'
            }

            driver = webdriver.Chrome(chrome_options=opts, executable_path=chromedriver_path)

            query_string = urlencode(payload)
            url = login_url + query_string
            driver.get(base_url + '?ipRedirectOverride=true')
            driver.get(url)
            if os.getenv("DEBUG") or debug:  # enable if you hit CAPTCHA or 2FA or other "security" screens
                print(
                    "You may need to login in a semi-automatic way, wait for the login screen to fail")
                search_box = driver.find_element_by_id('ap_email')
                search_box.send_keys(self.username)
                search_box = driver.find_element_by_id('ap_password')
                search_box.send_keys(self.password)
                search_box.submit()
                try:
                    search_box = driver.find_element_by_id('ap_password')
                    search_box.send_keys(self.password)
                except Exception as e:
                    print(e)
            else:
                search_box = driver.find_element_by_id('ap_email')
                search_box.send_keys(self.username)
                search_box = driver.find_element_by_id('ap_password')
                search_box.send_keys(self.password)
                search_box.submit()
                time.sleep(2)  # give the page some time to load

            print("Waiting for login")
            WebDriverWait(driver, 60).until(
                EC.presence_of_element_located((By.XPATH, "//a[@title='Blog']"))
            )
            print("Logged in")

            driver.get('https://www.audible.com/lib?ref_=a_hp_lib_tnaft_1')

            view_cnt = Select(driver.find_element_by_id('adbl_time_filter'))
            view_cnt.select_by_visible_text('All Time')
            time.sleep(5)

            item_cnt = Select(driver.find_element_by_id('adbl_items_per_page'))
            item_cnt.select_by_visible_text('200  items per page')
            time.sleep(5)
        else:
            driver = webdriver.Remote(command_executor=durl, desired_capabilities={})
            driver.session_id = sess

        print(driver.command_executor._url)
        print(driver.session_id)

        page_selected = int(driver.find_element_by_class_name('adbl-page-selected').text)
        print("Current Page: {}".format(page_selected))

        pagination_div = driver.find_element_by_class_name('adbl-pagination')
        soup = BeautifulSoup(pagination_div.get_attribute('outerHTML'), 'html.parser')
        page_last = int(soup.find('span', class_='adbl-page-next').previous_element)
        print("Last Page: {}".format(page_last))

        next_page = driver.find_element_by_xpath("(//*[@class='adbl-page-next']/a)[1]")

        books = {}
        for i in range(page_selected, page_last + 1):
            nextable = True
            try:
                next_page = driver.find_element_by_xpath("(//*[@class='adbl-page-next']/a)[1]")
            except Exception as e:
                nextable = False
                print(e)

            table_tr = driver.find_elements_by_xpath(
                "//*[@class='adbl-lib-content']/div/table/tbody/tr[@class=' odd' or @class='']")

            for tr in table_tr:
                tmp = tr.find_element_by_name('productId')
                prodid = tmp.get_attribute('value')
                book = {prodid: {'prodid': prodid}}

                tmp = tr.find_element_by_name('tdTitle')
                book[prodid]['title'] = str(tmp.text).replace(' (Unabridged)', '')

                tmp = tr.find_element_by_class_name('adbl-library-item-author')
                book[prodid]['author'] = tmp.text
                try:
                    tmp = tr.find_element_by_class_name('adbl-check-off-wfs')
                    if tmp.text == 'Downloaded':
                        book[prodid]['downloaded'] = True
                    else:
                        book[prodid]['downloaded'] = False
                except StaleElementReferenceException as stale:
                    print(stale)

                tmp = tr.find_elements_by_class_name('adbl-label')
                book[prodid]['length'] = tmp[0].text
                book[prodid]['date'] = tmp[1].text



                print("Title: {}   Author: {}  ProdID: {}".format(book[prodid]['title'], book[prodid]['author'], book[prodid]['prodid']))
                books.update(book)

            if nextable:
                print("Clicking next page")
                next_page.click()
                time.sleep(5)
                print("Waiting for page load")
                page_selected_wait = page_selected
                while page_selected == page_selected_wait:
                    try:
                        page_selected_wait = int(driver.find_element_by_class_name('adbl-page-selected').text)
                    except Exception as e:
                        print(e)
                    time.sleep(2)
                page_selected = page_selected_wait
                print("Page loaded")

        print(books)
        print(len(books))
        time.sleep(8)
        driver.quit()
        self.setBooksOnline(books)
        self.saveBooksOnlineDict()
        return books

    def saveBooksOnlineDict(self):
        if len(self.booksOnline) == 0:
            return False
        with open(self.booksOnlineJson, 'w') as fp:
            json.dump(self.booksOnline, fp, indent=4)
        return True

    def loadBooksOnlineDict(self):
        with open(self.booksOnlineJson, 'r') as fp:
            data = json.load(fp)
        self.booksOnline = data
        return data

    def setBooksOnline(self, books):
        self.booksOnline = books

    def getBooksOnline(self):
        return self.booksOnline
    #
    # Converter
    #
    def doConversion(self):
        queue = []
        runs = self.maxConversionsPerRun

        for bd in self.booksDownloaded:
            if bd not in self.booksConverted:
                if runs == 0:
                    print("Reached max conversions limit!")
                    break
                print("Queued for conversion: {}".format(os.path.join(self.audibleAaxPath, self.booksDownloaded[bd]['fname'])))
                queue.append(os.path.join(self.audibleAaxPath, self.booksDownloaded[bd]['fname']))
                runs -= 1

        print()
        for file in queue:
            print("Converting: {}".format(file))
            self.convert(file)

    def convert(self, file):
        print("Converting: {}".format(os.path.basename(file)))
        if len(self.activationCodes) == 0:
            self.getActivationCodes()

        meta = {}
        meta['prodid'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-ProductID', file])
        meta['releaseDate'] = parse(self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-ReleaseDate', file])).strftime("%Y:%m:%d")
        meta['artist'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-Artist', file])
        meta['album'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-Album', file])
        meta['title'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-Title', file])
        meta['year'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-ContentCreateDate', file])
        meta['comment'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-Description', file])
        meta['duration'] = self.runCmdGetVal(['exiftool', '-s', '-s', '-s', '-Duration', file])
        # pprint(meta)
        # exit(99)
        filename = "{} - {}.m4a".format( str(re.sub('[^-a-zA-Z0-9_.() ]+', '', meta['title'])).replace(' (Unabridged)', ''), re.sub('[^-a-zA-Z0-9_.() ]+', '', meta['artist']) )
        print("Output filename: {}".format(filename))
        rnd =randint(10000, 65534)
        temp_image = os.path.join(self.tempdir, str("{}-temp.png").format(rnd))
        temp_audio = os.path.join(self.tempdir, str("{}-temp.m4a").format(rnd))
        print("Temp Image: {}   Temp M4A: {}".format(temp_image, temp_audio))

        print("Extracting Cover-art...")
        self.runCmd(['ffmpeg', '-y', '-v', 'error', '-i', file, temp_image])

        print("Converting Audio...")
        for code in self.activationCodes:
            if self.runCmd(['ffmpeg', '-y', '-v', 'error', '-stats', '-activation_bytes', code, '-i', file, '-c:a', 'copy', '-vn', temp_audio], ['Error', 'Invalid', 'error', 'invalid']) is False:
                print('Error Occured trying next activation code')
                continue
            else:
                break

        print("Attaching Cover-art...")
        self.runCmd(['tageditor', 'set', '-v', 'cover={}'.format(temp_image), '--files', temp_audio])

        print("Adding Custom Metadata...")
        self.runCmd(['exiftool', '-config', 'exif.cfg', '-ProductID="{}"'.format(meta['prodid']), '-ReleaseDate="{}"'.format(meta['releaseDate']), '-Description="{}"'.format(meta['comment']), temp_audio])

        print("Moving to converted...")
        shutil.move(temp_audio, os.path.normpath(os.path.join(self.audibleM4aPath, filename)))

        print("Cleaning up...")
        self.cleanup()

        print("Converted to {}".format(os.path.normpath(os.path.join(self.audibleM4aPath, filename))))

        aaxfile = os.path.split(file)

        os.rename(file, "{}".format(os.path.join(aaxfile[0], "CONVERTED-{}.old".format(aaxfile[1]))))
        print("Renamed {} to {}".format(os.path.basename(file), "CONVERTED-{}.old".format(aaxfile[1])))

        print("Updating {}".format(self.booksConvertedJson))
        book = {
            'title': meta['title'],
            'author': meta['artist'],
            'length': meta['duration'],
            'date': meta['releaseDate'],
            'prodid': meta['prodid'],
            'fname': filename
        }
        self.booksConverted[meta['prodid']] = book
        self.saveBooksConvertDict()
        for x in range(1, 5):
            print()

    def getActivationCodes(self):
        data = self.runCmdGetVal('get-devices.cmd')
        print("Activation Codes: {}".format(data.split(',')))
        self.activationCodes = data.split(',')
        return data.split(',')

    def runCmd(self, cmd, catcherr = None):
        print(cmd)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in p.stdout.readlines():
            if catcherr is None:
                print(line.decode('utf-8'))
            else:
                print(line.decode('utf-8'))
                if any(x in line.decode('utf-8') for x in catcherr):
                    p.kill()
                    return False

        retval = p.wait()
        return None

    def runCmdGetVal(self, cmd):
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in p.stdout.readlines():
            if line.decode("utf-8") != '':
                return str(line.decode()).rstrip().replace('\r\n', '').replace('\n', '').replace('\r', '')
        retval = p.wait()

    #
    #Downloads
    #
    def getDownloadsNeeded(self):
        books = []
        booksjson = {}

        for bo in self.booksOnline:
            if bo in self.booksConverted:
                continue
            if bo in self.booksDownloaded:
                continue
            books.append("{} - {}  {}".format(self.booksOnline[bo]['title'], self.booksOnline[bo]['author'], self.booksOnline[bo]['date'] ))
            booksjson.update({bo:{}})
            booksjson[bo] = self.booksOnline[bo]

        books = sorted(books)
        with open('BooksToDownload.txt', 'w') as fp:
            fp.write("Title - Author  Date Purchased \n")
            fp.write("Total to download = {}\n\n".format(len(books)))
            for bo in books:
                # print("{}".format(bo))
                fp.write("{}\n".format(bo))

        print("BooksToDownload.txt has been created!")

        with open(self.booksToDownloadJson, 'w') as fp:
            json.dump(booksjson, fp, indent=4)

    def downloadBooks(self):
        if not os.path.isfile(self.booksToDownloadJson):
            self.getDownloadsNeeded()
        else:
            print(colored("Delete {} to cause the books to download list to refresh.".format(self.booksToDownloadJson), 'green'))

        if not self.username or not self.password:
            raise Exception("Username or Password not set")

        books = self.loadBooksToDownloadDict()
        self.cleanup()

        base_url = 'https://www.audible.com/'
        chromedriver_path = "chromedriver.exe"
        debug = True

        durl = None #'http://127.0.0.1:53864'
        sess = None #'eac9a56c66cde2280ec1f5a205a0bab9'

        if sess is None:
            opts = webdriver.ChromeOptions()
            prefs = {"profile.default_content_settings.popups": 0,
                     "download.default_directory": r"{}\\".format(self.tempdir),
                     # IMPORTANT - ENDING SLASH V IMPORTANT
                     "directory_upgrade": True}
            opts.add_argument(
                "user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0"
            )
            opts.add_experimental_option("prefs", prefs)

            if '@' in self.username:  # Amazon login using email address
                login_url = "https://www.amazon.com/ap/signin?"
            else:  # Audible member login using username (untested!)
                login_url = "https://www.audible.com/sign-in/ref=ap_to_private?forcePrivateSignIn=true&rdPath=https%3A%2F%2Fwww.audible.com%2F%3F"

            # player_id = base64.encodestring(hashlib.sha1("".encode('utf8')).digest()).rstrip()  # keep this same to avoid hogging activation slots
            player_id = b'2jmj7l5rSw0yVb/vlWAYkK/YBwk='
            print("[*] Player ID is %s" % player_id)  # 2jmj7l5rSw0yVb/vlWAYkK/YBwk=

            payload = {
                'openid.ns': 'http://specs.openid.net/auth/2.0',
                'openid.identity': 'http://specs.openid.net/auth/2.0/identifier_select',
                'openid.claimed_id': 'http://specs.openid.net/auth/2.0/identifier_select',
                'openid.mode': 'logout',
                'openid.assoc_handle': 'amzn_audible_us',
                'openid.return_to': base_url + 'player-auth-token?playerType=software&playerId=%s=&bp_ua=y&playerModel=Desktop&playerManufacturer=Audible' % (
                player_id)
                # 'openid.return_to': base_url + 'player-auth-token?playerType=software&playerId=%s=&bp_ua=y&playerModel=Desktop&playerManufacturer=Audible'
            }

            driver = webdriver.Chrome(chrome_options=opts, executable_path=chromedriver_path)

            query_string = urlencode(payload)
            url = login_url + query_string
            driver.get(base_url + '?ipRedirectOverride=true')
            driver.get(url)
            if os.getenv("DEBUG") or debug:  # enable if you hit CAPTCHA or 2FA or other "security" screens
                print(
                    "You may need to login in a semi-automatic way, wait for the login screen to fail")
                search_box = driver.find_element_by_id('ap_email')
                search_box.send_keys(self.username)
                search_box = driver.find_element_by_id('ap_password')
                search_box.send_keys(self.password)
                search_box.submit()
                try:
                    search_box = driver.find_element_by_id('ap_password')
                    search_box.send_keys(self.password)
                except Exception as e:
                    print(e)
            else:
                search_box = driver.find_element_by_id('ap_email')
                search_box.send_keys(self.username)
                search_box = driver.find_element_by_id('ap_password')
                search_box.send_keys(self.password)
                search_box.submit()
                time.sleep(2)  # give the page some time to load

            print("Waiting for login")
            WebDriverWait(driver, 60).until(
                EC.presence_of_element_located((By.XPATH, "//a[@title='Blog']"))
            )
            print("Logged in")

            driver.get('https://www.audible.com/lib?ref_=a_hp_lib_tnaft_1')

            view_cnt = Select(driver.find_element_by_id('adbl_time_filter'))
            view_cnt.select_by_visible_text('All Time')
            time.sleep(5)

            item_cnt = Select(driver.find_element_by_id('adbl_items_per_page'))
            item_cnt.select_by_visible_text('200  items per page')
            time.sleep(5)
        else:
            driver = webdriver.Remote(command_executor=durl, desired_capabilities={})
            driver.session_id = sess

        print(driver.command_executor._url)
        print(driver.session_id)

        page_selected = int(driver.find_element_by_class_name('adbl-page-selected').text)
        print("Current Page: {}".format(page_selected))

        pagination_div = driver.find_element_by_class_name('adbl-pagination')
        soup = BeautifulSoup(pagination_div.get_attribute('outerHTML'), 'html.parser')
        page_last = int(soup.find('span', class_='adbl-page-next').previous_element)
        print("Last Page: {}".format(page_last))

        next_page = driver.find_element_by_xpath("(//*[@class='adbl-page-next']/a)[1]")

        runs = self.maxDownloadsPerRun

        for i in range(page_selected, page_last + 1):
            nextable = True
            try:
                next_page = driver.find_element_by_xpath("(//*[@class='adbl-page-next']/a)[1]")
            except Exception as e:
                nextable = False
                print(e)

            table_tr = driver.find_elements_by_xpath(
                "//*[@class='adbl-lib-content']/div/table/tbody/tr[@class=' odd' or @class='']")

            for tr in table_tr:
                tmp = tr.find_element_by_name('productId')
                prodid = tmp.get_attribute('value')

                # print("Looking for prodid: {}".format(prodid))
                # print(books)
                if prodid in books:
                    if runs == 0:
                        print('Reached max download limit!')
                        time.sleep(8)
                        driver.quit()
                        return True
                    tmp = tr.find_element_by_name('tdTitle')
                    print("Found: {}".format(tmp.text))
                    tmp = tr.find_element_by_class_name('adbl-download-it')
                    tmp.click()
                    ctime = int(time.time())
                    while not os.path.isfile(os.path.join(self.tempdir,'admhelper.adh')):
                        if (int(time.time()) - 60) > ctime:
                            raise Exception("Timeout waiting for download file to show up!")
                    try:
                        os.rename(os.path.join(self.tempdir,'admhelper.adh'), os.path.join(self.tempdir,'{}.adh'.format(prodid)))
                    except FileExistsError:
                        pass
                    runs -= 1

            if nextable:
                print("Clicking next page")
                next_page.click()
                time.sleep(5)
                print("Waiting for page load")
                page_selected_wait = page_selected
                while page_selected == page_selected_wait:
                    try:
                        page_selected_wait = int(driver.find_element_by_class_name('adbl-page-selected').text)
                    except Exception as e:
                        print(e)
                    time.sleep(2)
                page_selected = page_selected_wait
                print("Page loaded")


        time.sleep(8)
        driver.quit()

    def loadBooksToDownloadDict(self):
        with open(self.booksToDownloadJson, 'r') as fp:
            data = json.load(fp)
        return data

    def AudibleDownload(self):
        dldcnt = len(self.booksDownloaded)
        todl = 0
        for root, dirs, files in os.walk(self.tempdir):
            print("Books to download = {}".format(len(files)))
            todl = len(files)
            for f in files:
                app = Application().start("{} {}".format(self.audibleDownloadHelperPath, os.path.join(self.tempdir, f)), timeout=5)

        spins = ['Waiting for all downloaded: |', 'Waiting for all downloaded: /', 'Waiting for all downloaded: -', 'Waiting for all downloaded: \\']
        x = 0
        cnt = 0
        dldcnt += todl
        done = True
        while done:
            for i in spins:
                sys.stdout.write("\r" + i)
                sys.stdout.flush()

                if x >= 10:
                    self.getDownloadedBooks(True)
                    cnt = len(self.booksDownloaded)
                    x = 0

                if cnt == dldcnt:
                    print()
                    print("All Downloaded")
                    done = False
                time.sleep(1)
                x += 1

        app = Application().connect(path=self.audibleDownloadHelperPath)
        app.kill()
        self.cleanup()

    #
    #Utils
    #
    def setup(self):
        if not os.path.isfile('settings.json'):
            print(colored("\nThis is the first run. Please fill in the settings.json file that was just created.", 'red'))
            settings = {'AudibleAaxDir': '',
                        'AudibleAaxDirExample': 'Example:  C:/Users/Justin/Documents/Audible-ToConvert   (backslashs must be escaped)',
                        'AudibleM4aDir': '',
                        'AudibleM4aDirExample': 'Example:  C:\\Users\\Justin\\Documents\\Audible-Converted   (backslashs must be escaped)',
                        'AudibleDownloadHelperPath': 'C:/Program Files (x86)/Audible/Bin/AudibleDownloadHelper.exe',
                        'AudibleDownloadHelperPathExample': 'Example:  C:/Program Files (x86)/Audible/Bin/AudibleDownloadHelper.exe   (backslashs must be escaped)',
                        'AudibleUsername': '',
                        'AudiblePassword': '',
                        'MaxConversionsPerRun': 100,
                        'MaxDownloadsPerRun': 20
                        }
            with open('settings.json', 'w') as fp:
                json.dump(settings, fp, indent=4)
            exit(1)
        else:
            with open('settings.json', 'r') as fp:
                data = json.load(fp)
                print("Settings: {}".format(data))
            self.audibleAaxPath = os.path.normpath(data['AudibleAaxDir'])
            self.audibleM4aPath = os.path.normpath(data['AudibleM4aDir'])
            self.audibleDownloadHelperPath = os.path.normpath(data['AudibleDownloadHelperPath'])
            self.username = data['AudibleUsername']
            self.password = data['AudiblePassword']
            self.maxConversionsPerRun = data['MaxConversionsPerRun']
            self.maxDownloadsPerRun = data['MaxDownloadsPerRun']

    def cleanup(self, rmDir=False):
        if rmDir:
            shutil.rmtree(self.tempdir)
        else:
            for root, dirs, files in os.walk(self.tempdir):
                for f in files:
                    try:
                        os.unlink(os.path.join(root, f))
                    except PermissionError:
                        pass
                    except Exception:
                        pass




#
# MAIN
#
try:
    assert sys.version_info >= (3,3)
except AssertionError:
    print(colored('Python 3.3 or newer required to run this script!', 'red'))
    exit(1)

parser = argparse.ArgumentParser(description='Select only one of the modes below.')
modes = parser.add_argument_group('Modes')

modes.add_argument('-f', '--full', help='Do the full process', action='store_true')
modes.add_argument('-s', '--scrape', help='Only scrape Audible and update booksOnline.json', action='store_true')
modes.add_argument('-u', '--update', help='Update the booksDownloaded.json and booksConverted.json files. Will update online if booksOnline.json is older that 24 hrs.', action='store_true')
modes.add_argument('-c', '--convert', help='Only do conversions', action='store_true')
modes.add_argument('-k', '--list_only', help='Get list of books needing downloaded', action='store_true')
modes.add_argument('-l', '--list_update', help='Update then get list of books needing downloaded', action='store_true')
modes.add_argument('-d', '--download', help='Download books missing from converted list', action='store_true')

if not len(sys.argv) > 1:
    parser.print_help()
    exit(1)

args = parser.parse_args()
c = 0
for i in vars(args):
    if getattr(args, i):
        c += 1
if c > 1:
    parser.print_help()
    print()
    print(colored('Too may modes selected. Select only one!', 'red'))
    exit(1)

ac = AudibleConverter()

if args.full:
    ac.getOnlineLibrary(True)
    ac.getDownloadedBooks()
    ac.getConvertedBooks()
    ac.getDownloadsNeeded()
    ac.downloadBooks()
    ac.AudibleDownload()
    ac.doConversion()
    ac.getConvertedBooks()

if args.scrape:
    ac.getOnlineLibrary(True)

if args.update:
    ac.getOnlineLibrary()
    ac.getDownloadedBooks()
    ac.getConvertedBooks()

if args.download:
    ac.getDownloadsNeeded()
    ac.downloadBooks()
    ac.AudibleDownload()

if args.list_only:
    ac.getDownloadsNeeded()

if args.list_update:
    ac.getOnlineLibrary()
    ac.getDownloadedBooks()
    ac.getConvertedBooks()
    ac.getDownloadsNeeded()

if args.convert:
    ac.doConversion()

ac.cleanup()

print("DONE")